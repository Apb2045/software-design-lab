/**
 * Created by Alex on 10/23/15.
 */

//import javax.swing.*;

import java.awt.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class test extends JPanel {

    private JFrame f;
    private JPanel p;
    private JButton b1;
    private JButton b2;
    private JLabel lab;
    private GridBagConstraints gc;
    private Random rand;
    private Graphics g;

    public test()
    {
         //3. test calls the gui function to create the gui
    }
/*
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        g.setColor(Color.cyan);
        int startx = rand.nextInt(500);
        int starty = rand.nextInt(500);
        int width = rand.nextInt(550);
        int height = rand.nextInt(550);

        g.drawRect(startx, starty, width, height);
    }
*/

    public void gui()
    {
        // 4. set up the frame and name it, make visible, set size, and make it close
        f = new JFrame("Creativity tuts");
        f.setVisible(true);
        f.setSize(800, 800);
        f.setResizable(false);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //5. set up new panel which sits on top of the frame, give it a color
        p = new JPanel(new GridBagLayout()); //creating gridbag layout to help us arrange the buttons
        //p.setBackground(Color.LIGHT_GRAY);

        //6. create a button and a label
        b1 = new JButton(" Quit  ");
        b2 = new JButton("Redraw");
        gc = new GridBagConstraints();
        //lab = new JLabel("This is test label");




        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        rand = new Random();

        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                g.drawRect(18,18,18,18);
                g.setColor(Color.GREEN);
            }
        });

        //7. add the button and label to the panel
        gc.insets = new Insets(5,5,5,5);
        gc.gridx = 0;
        gc.gridy = 2;
        p.add(b1,gc);
        gc.gridx = 0;
        gc.gridy = 1;
        p.add(b2,gc);
        //p.add(lab);

        //8. add the panel to the frame
        f.add(p,BorderLayout.EAST);
    }

    public static void main(String[] args) //1. Start here
    {
        test t = new test();
        t.gui();//2. creates a new test object calling the default constructor for the class
    }
}
