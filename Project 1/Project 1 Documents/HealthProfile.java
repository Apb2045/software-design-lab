//Klaudio Vito
//CSC 221 / R - Programming Project 1
//Health Profile
//Date: 09/08/2015
import javax.swing.JOptionPane;
import java.util.*;
//I will not be using "Scanner" utility since I chose to use "JOptionPane" to display dialog boxes to get input 

class HealthProfileFunctions {
	public String FirstName, LastName, Gender; //declare necessary strings for the class
	public int BirthDay, BirthMonth, BirthYear, Age; //declare necessary integers for the class
	public double  Height, Weight, MaxHeartRate, TargetHeartRate1, TargetHeartRate2, BMI; //declares necessary doubles for the class
	
	/*
	 * Getting information input from the user
	 */
	
	//Get the first and last name from user
	public void setFirstName() {
		//shows a dialog box that prompts user to enter first name
		FirstName = JOptionPane.showInputDialog("Please enter your Name: "); 		
	}
	
	public void setLastName()
	{
		//shows a dialog box that prompts user to enter last name
		LastName = JOptionPane.showInputDialog("Please enter your Last Name: ");
	}
	
	//Get the gender from user
	public void setGender() {
		//validity boolean variable
		boolean valid = false; 
		//while loop so that user can re-input gender if mistyped
		while (!valid) {
			//shows a dialog box that prompts the user to enter gender
			Gender = JOptionPane.showInputDialog("Please enter your Gender: ");
			//puts the string Gender to lowercase so there are less cases
			Gender = Gender.toLowerCase();
			//if-else statement to check if the input is correct
			if (Gender.matches("male") || Gender.matches("m") || Gender.matches("female") || Gender.matches("f"))
				valid = true;
			else {
				JOptionPane.showMessageDialog(null,"Incorrect input! Try agains: ");
				valid = false;
			}	
		}	
	}
	
	//function to set birth-date from user
	public void setBirthDate() {
		//dialog boxes to get user's information
		BirthMonth = Integer.parseInt(JOptionPane.showInputDialog("Please enter your birth MONTH: "));
		BirthDay = Integer.parseInt(JOptionPane.showInputDialog("Please enter your birth DAY: "));
		BirthYear = Integer.parseInt(JOptionPane.showInputDialog("Please enter your birth YEAR: "));
	}
	
	//function to test if the input date is correct, using Gregorian Calendar
	public boolean testDate() {
		try {
            GregorianCalendar gc = new GregorianCalendar();
            gc.setLenient(false);        // must do this
            gc.set(GregorianCalendar.YEAR, BirthYear);
            gc.set(GregorianCalendar.MONTH, BirthMonth);
            gc.set(GregorianCalendar.DATE, BirthDay);
            
            gc.getTime(); // exception thrown here
            return true;
        }
        catch (Exception e) {
            return false;
        }
	}
	
	/*
	 * function to get the Age of the user
	 * this function will run only if the testDate function is true
	 * if the user's birthday hasn't come yet this year, we have to subtract from 2014
	 * if the user's birthday is already passed than we have to subtract from 2015
	 * if input is not correct the program will exit
	 */
	public int getAge() {
		boolean valid = false;
		if (testDate()) {
			if(BirthMonth >= 9 && BirthDay >= 8) {
				Age = 2014 - BirthYear;
			}
			else {
				Age = 2015 - BirthYear;
			}
		}
		else {
			while(!valid)
			{
				JOptionPane.showMessageDialog(null,"Incorrect Birth Date! Try Again.");
				setBirthDate();
				if(testDate())
					valid = true;
			}
		}
		return Age;
	}
	
	//function to get height from user
	public void setHeight() {
		//validity variable
		boolean valid = false;
		//prompt the user to enter the height until it is a number using try-catch method
		while (!valid) {
			try {
				Height = Double.parseDouble (JOptionPane.showInputDialog("Please enter your Height in inches: "));
				valid = true;
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null,"Incorrect input! Try again: ");
			}
		}
	}
	
	//same thing as the setHeight function, but for the weight
	public void setWeight () {
		boolean valid = false;
		while (!valid) {
			try {
				Weight = Double.parseDouble (JOptionPane.showInputDialog("Please enter your Weight in pounds: "));
				valid = true;
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null,"Incorrect input! Try again: ");
			}
		}
	}

	/*
	 * return functions
	 */
	
	//returns first name to caller
	public String getFirstName() {
		return FirstName; 
	}
	
	//returns last name to caller
	public String getLastName() {
		return LastName; 
	}

	//function that calculates and returns maximum heart rate to the user
	public double getMaxHeartRate() {
			MaxHeartRate = 220 - getAge(); 
			return MaxHeartRate;
	}
	
	//function to get minimum bound on Target heart rate and return it to caller
	public double getTargetHeartRate1() {
			TargetHeartRate1 = MaxHeartRate * 0.5; 
			return TargetHeartRate1; 
	}
	
	//function to get maximum bound on Target heart rate and return it to caller
	public double getTargetHeartRate2() {
			TargetHeartRate2 = MaxHeartRate * 0.85; 
			return TargetHeartRate2;
	}
	
	//function to calculate and return BMI to caller
	public double getBMI() {
		BMI = (Weight/(Height * Height)) * 703;
		return BMI;
	}
	
	//shows a dialog box to tell the user the condition of his/her BMI
	public String showInfo() {
		String message;
		if(getBMI() <= 19.5)
			message =  "Be careful, you are underweight!";
		else if(getBMI() >= 24.9 && getBMI() <= 29.9)
			message = "Be careful, you are overweight!";
		else if(getBMI() > 30)
			message = "Be very careful, you are obese!";
		else
			message = ("Nice! You are in shape!");
		return message;
	}
}


public class HealthProfile {
	
	
	public static void main (String[] args) {	
		
		HealthProfileFunctions account = new HealthProfileFunctions(); //creates an object from the HealthProfileFunctions class
		
		account.setFirstName();
		account.setLastName();
		account.setGender();
		account.setBirthDate();
		account.getAge();
		account.setHeight();
		account.setWeight();
		
		JOptionPane.showMessageDialog(null, account.getFirstName()+ " " + account.getLastName() + "'s " + " Health Profile" + "\n" +
				"Date of Birth: " + account.BirthDay + "/" + account.BirthMonth + "/" + account.BirthYear + "\n" +
				"Age: " + account.getAge() + "\n" +
				"Gender: " + account.Gender + "\n" +
				"Height: " + account.Height + " inches \n" +
				"Weight: " + account.Weight + " pounds \n" +
				"Maximum heart rate is: " + account.getMaxHeartRate() + " beats/minute \n" +
				"Target heart rate is between " + account.getTargetHeartRate1() + " and " + account.getTargetHeartRate2() + " beats/minute \n" +
				"Body Mass index: " + account.getBMI() + "\n" + 
				account.showInfo()); //shows dialog box with information about the user using JOptionPane and not System.out
			
	}
}