package HealthProfile; /**
 * Created by Alex on 9/7/15.
 */
import java.util.Calendar;
import java.util.Scanner;

public class Health {

    private String firstName;
    private String lastName;
    private char sex;
    private int birthMonth;
    private int birthDay;
    private int birthYear;
    private double height;
    private double weight;
    private double BMI;
    private int maxHeartRate;
    private int targetHeartRate;
    private int targetHeartRateLower;
    private int targetHeartRateUpper;
    private int age;
    private int Year = Calendar.getInstance().get(Calendar.YEAR);

    Scanner input = new Scanner(System.in);

    public Health(){
        firstName = "";
        lastName = "";
        sex = ' ';
        birthMonth = 1;
        birthDay = 1;
        birthYear = 1900;
        height = 0;
        weight = 0;
        maxHeartRate =0;
        targetHeartRate =0;
        targetHeartRateLower =0;
        targetHeartRateUpper =0;
        age = 0;
    }

    public void setFirstName(String s){
        this.firstName =s;
    }
    public void setLastName(String s){
        this.lastName =s;
    }
    public void setGender(char c){
        if(c == 'm' || c == 'f' || c == 'M' || c == 'F'){
            this.sex =c;}
        else{
            System.out.print("Invalid sex, please enter (m/f): ");
            this.setGender(input.next().charAt(0));}
    }
    public void setBirthYear(int i ){
        if(i >=1900 && i <= 2015){
            this.birthYear =i;}
        else{
            System.out.print("Invalid year, please enter year from 1900 to 2015: ");
            this.setBirthYear(input.nextInt());}
    }
    public void setBirthMonth(int i){
        if(i >= 1 && i <= 12){
            this.birthMonth =i;}
        else{
            System.out.print("Invalid month, please enter month from 1 to 12: ");
            this.setBirthMonth(input.nextInt());}
    }
    public void setBirthDay(int i){
        if(i >= 1 && i <=31 && (this.birthMonth == 1 || this.birthMonth ==  3 ||this.birthMonth ==  5 || this.birthMonth == 7 ||this.birthMonth ==  8 || this.birthMonth == 10 ||this.birthMonth ==  12)){
            this.birthDay = i;}
        else if(i >=1 && i <= 30 && (this.birthMonth == 4 || this.birthMonth == 6 || this.birthMonth == 9 || this.birthMonth == 11)){
            this.birthDay = i;}
        else if(this.birthMonth == 2 && (this.birthYear%4 != 0 || this.birthYear == 1900) && i >= 1 && i <= 28){
            this.birthDay = i;}
        else if (this.birthMonth == 2 && this.birthYear%4 == 0 && this.birthYear != 1900 && i>= 1 && i<= 29){
            this.birthDay = i;}
        else{
            System.out.print("Invalid day, please enter valid date for month " + this.birthMonth + ": ");
            this.setBirthDay(input.nextInt());}
    }
    public void setHeight(double d){
        if(d > 0){
            this.height = d;}
        else{
            System.out.print("Invalid height, please enter height greater than 0 inches: ");
            this.setHeight(input.nextDouble());}
    }
    public void setWeight(double d){
        if(d > 0){
            this.weight = d;}
        else{
            System.out.print("Invaid weight, please enter weight greater than 0 pounds: ");
            this.setWeight(input.nextDouble());}
    }
    public void setAge(){
        this.age = Year - this.birthYear;
    }
    public void setMaxHeartRate(){
        this.maxHeartRate = 220 - this.age;
    }
    public void setTargetHeartRate(){
        this.targetHeartRateLower = maxHeartRate/2;
        this.targetHeartRateUpper = maxHeartRate*17/20;
    }
    public void setBMI(){
        this.BMI = weight/(height*height)*703;
    }

    public String getFirstName(){
        return firstName;
    }
    public String getLastName(){
        return lastName;
    }
    public char getSex(){
        return sex;
    }
    public int getBirthYear(){
        return birthYear;
    }
    public int getBirthMonth(){
        return birthMonth;
    }
    public int getBirthDay(){
        return birthDay;
    }
    public double getHeight(){
        return height;
    }
    public double getWeight(){
        return weight;
    }
    public int getAge(){
        return age;
    }
    public int getMaxHeartRate(){
        return maxHeartRate;
    }
    public int getTargetHeartRateLower(){
        return targetHeartRateLower;
    }
    public int getTargetHeartRateUpper(){
        return targetHeartRateUpper;
    }
    public double getBMI(){
        return BMI;
    }

}
