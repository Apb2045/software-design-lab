package HealthProfile; /**
 * Created by Alex on 9/7/15.
 */

import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        Health patient = new Health();
        DecimalFormat dec = new DecimalFormat("#.##");
        String line;
        int digit = -1;
        double decimal;

        System.out.print("Enter First Name: ");
        patient.setFirstName(input.nextLine());
        System.out.print("Enter Last Name: ");
        patient.setLastName(input.nextLine());
        System.out.print("Enter Sex (m/f): ");
        patient.setGender(input.next().charAt(0));

        System.out.print("Enter Birth Year: ");
        input.nextLine();
        line = input.nextLine();

        while(true){
            try{
                digit = Integer.parseInt(line);
                break;
            } catch(NumberFormatException e){}
            System.out.print("You have entered a non numerical character. Please enter an integer: ");
            line = input.nextLine();
        }
        patient.setBirthYear(digit);


        System.out.print("Enter Birth Month: ");
        line = input.nextLine();

        while(true){
            try{
                digit = Integer.parseInt(line);
                break;
            } catch(NumberFormatException e){}

            System.out.print("You have entered a non numerical character. Please enter an integer: ");
            line = input.nextLine();
        }
        patient.setBirthMonth(digit);

        System.out.print("Enter Birth Day: ");
        line = input.nextLine();

        while(true){
            try{
                digit = Integer.parseInt(line);
                break;
            } catch(NumberFormatException e){}

            System.out.print("You have entered a non numerical character. Please enter an integer: ");
            line = input.nextLine();
        }
        patient.setBirthDay(digit);

        System.out.print("Enter Height (in): ");
        line = input.nextLine();

        while(true){
            try{
                decimal = Double.parseDouble(line);
                break;
            } catch(NumberFormatException e){}

            System.out.print("You have entered a non numerical character. Please enter a double: ");
            line = input.nextLine();
        }
        patient.setHeight(decimal);

        System.out.print("Enter Weight (lb): ");
        line = input.nextLine();

        while(true){
            try{
                decimal = Double.parseDouble(line);
                break;
            } catch(NumberFormatException e){}

            System.out.print("You have entered a non numerical character. Please enter a double: ");
            line = input.nextLine();
        }
        patient.setWeight(decimal);


        patient.setAge();
        patient.setMaxHeartRate();
        patient.setTargetHeartRate();
        patient.setBMI();

        System.out.println(" ");
        System.out.println(" ");

        System.out.println("HEALTH PROFILE FOR: " + patient.getLastName() + ", " + patient.getFirstName());
        System.out.print("DOB: " + patient.getBirthMonth() + "/" + patient.getBirthDay() + "/" + patient.getBirthYear());
        System.out.println("   AGE: " + patient.getAge() + "   SEX: " + patient.getSex());
        System.out.println( "MAXIMUM HEART RATE: " + patient.getMaxHeartRate());
        System.out.println("TARGET HEART RATE: " + patient.getTargetHeartRateLower() + "-" + patient.getTargetHeartRateUpper());
        System.out.println("BMI: " + dec.format(patient.getBMI()));
    }
}
