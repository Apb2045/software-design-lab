import java.util.*;

/**
 * Created by Alex on 10/18/15.
 */
public class library {

    public static void main(String args[]) {
        ComparatorChain chain = new ComparatorChain();
        Database library = new Database();
        Calendar cal = Calendar.getInstance();
/*
        class sortByAddedOn{
            sortByAddedOn(){};
        }

        class sortByDirector{
            sortByDirector(){};
        }
*/
/*
        class sortByTitle implements Comparator {
            public int compare(Object o1, Object o2) {
                if (o1 instanceof item && o2 instanceof item) {
                    String s1 = ((item) o1).getTitle();
                    String s2 = ((item) o2).getTitle();
                    Integer i1 = new Integer(s1.substring(s1.indexOf("-"), s1.length()));
                    Integer i2 = new Integer(s2.substring(s2.indexOf("-"), s2.length()));
                    return i1.compareTo(i2);
                }
                return 0;
            }
        }
*/
        class sortByID implements Comparator <item>{
            public int compare(item I1, item I2) {
                return I1.getId().compareTo(I2.getId());
            }
        }
        class sortByTitle implements Comparator <item>{
            public int compare(item I1, item I2) {
                return I1.getTitle().compareTo(I2.getTitle());
            }
        }
        class sortByAddedOn implements Comparator <item>{
            public int compare(item I1, item I2) {
                return I1.getAddedOn().compareTo(I2.getAddedOn());
            }
        }
        class sortByDirector implements Comparator <item>{
            public int compare(item I1, item I2) {
                if(I1.getClass().equals(textbook.class)){return 0;}
                if(I1.getClass().equals(cd.class)){return 0;}
                return I1.getDirector().compareTo(I2.getDirector());
            }
        }


        // adding database entries
        cal.set(1890, Calendar.AUGUST, 10);
        Date date = (Date) cal.getTime();
        library.addItem(new textbook("TB15", "TextX", date, "John Doe"));

        cal.set(1954, Calendar.JANUARY, 18);
        date = (Date) cal.getTime();
        library.addItem(new video("V09", "VideoB", date, 70000, "J. Smith"));

        cal.set(2000, Calendar.FEBRUARY, 29);
        date = (Date) cal.getTime();
        library.addItem(new textbook("TB01", "TextY", date, "John Doe"));

        cal.set(2000, Calendar.FEBRUARY, 29);
        date = (Date) cal.getTime();
        library.addItem(new cd("CD07", "CD1", date, 1000, "B.D."));

        cal.set(1990, Calendar.APRIL, 30);
        date = (Date) cal.getTime();
        library.addItem(new cd("CD10", "CD1", date, 800, "X.Y."));

        cal.set(2000, Calendar.FEBRUARY, 29);
        date = (Date) cal.getTime();
        library.addItem(new cd("CD05", "CD1", date, 1000, "B.C."));

        cal.set(1890, Calendar.JULY, 2);
        date = (Date) cal.getTime();
        library.addItem(new video("V12", "VideoA", date, 7000, "Joe Smith"));

        // print unsorted database
        System.out.println("----- DATABASE BEFORE SORTING: -----\n");
        library.list();
        // sort and print sorted database (by id)
        sortByID sortID = new sortByID();
        Collections.sort(library.items,sortID);
        System.out.println("----- DATABASE AFTER SORTING BY ID (default): -----\n");
        library.list();
        // sort by other fields
        System.out.println("----- DATABASE AFTER SORTING BY OTHER FIELDS: -----");
        System.out.println("------------ (title, addedOn, director) -----------\n");
        chain.addComparator(new sortByTitle());
        chain.addComparator(new sortByAddedOn());
        chain.addComparator(new sortByDirector());
        Collections.sort(library.items, chain);
        library.list();
    }

}
