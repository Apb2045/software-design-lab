/**
 * Created by Alex on 10/19/15.
 */

import java.util.ArrayList;
import java.util.List;

public class Database {

    protected List<item> items;
    protected List<String> listed;
    protected String outString = "";

    public Database() {items = new ArrayList<item>();}

    public void addItem(item item){items.add(item);}

    public void list(){
        System.out.println("Director, Artist, Title, Added On, ID, Playing Time, Author\n");

        for(item i: items){
           // if(i.getClass().equals(cd.class)){
                System.out.println(i.getDirector() + ", " + i.getArtist() + ", "
                        + i.getTitle() + ", " + i.getAddedOn() + ", " + i.getId() + ", "
                        + i.getPlayingTime() + ", " + i.getAuthor());
           // }
        }
    };

}
