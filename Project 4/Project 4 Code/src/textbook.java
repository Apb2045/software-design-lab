import java.util.Date;

/**
 * Created by Alex on 10/14/15.
 */
public class textbook extends item {

    protected String author;

    public textbook(String my_id, String my_title, Date my_addedOn, String my_author){
        super(my_id, my_title, my_addedOn);
        id = my_id;
        title = my_title;
        addedOn = my_addedOn;
        author = my_author;
    }

    @Override
    public int compareTo(item o) {
        return 0;
    }

    @Override
    String getId() {
        return id;
    }

    @Override
    String getTitle() {
        return title;
    }

    @Override
    Date getAddedOn() {
        return addedOn;
    }

    @Override
    String getDirector() {
        return null;
    }

    @Override
    String getArtist() {
        return null;
    }

    @Override
    int getPlayingTime() {
        return 0;
    }

    @Override
    String getAuthor() {
        return author;
    }

    @Override
    void setId(String id) {

    }

    @Override
    void setTitle(String title) {

    }

    @Override
    void setAddedOn() {

    }
}
