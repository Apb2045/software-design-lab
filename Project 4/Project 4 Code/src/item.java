import java.util.Date;

/**
 * Created by Alex on 10/14/15.
 */
abstract class item implements Comparable <item> {
    protected String id;
    protected String title;
    protected Date addedOn;

    public item(String my_id, String my_title, Date my_addedOn){
        super();
        id = my_id;
        title = my_title;
        addedOn = my_addedOn;
    }

    //need to define the functions that are used in the subclasses
    abstract String getId();
    abstract String getTitle();
    abstract Date getAddedOn();
    abstract String getDirector();
    abstract String getArtist();
    abstract int getPlayingTime();
    abstract String getAuthor();
    abstract void setId(String id);
    abstract void setTitle(String title);
    abstract void setAddedOn();

}
