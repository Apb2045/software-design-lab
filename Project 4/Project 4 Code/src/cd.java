import java.util.Date;

/**
 * Created by Alex on 10/14/15.
 */
public class cd extends multiMediaItem {

    private String artist;

    public cd(String my_id, String my_title, Date my_addedOn, int my_playingTime, String my_artist){
        super(my_id, my_title, my_addedOn, my_playingTime);
        id = my_id;
        title = my_title;
        addedOn = my_addedOn;
        playingTime = my_playingTime;
        artist = my_artist;
    }

    @Override
    public int compareTo(item o) {
        return 0;
    }

    @Override
    String getId() {
        return id;
    }

    @Override
    String getTitle() {
        return title;
    }

    @Override
    Date getAddedOn() {
        return addedOn;
    }

    @Override
    String getDirector() {
        return null;
    }

    @Override
    String getArtist() {
        return artist;
    }

    @Override
    void setId(String id) {

    }

    @Override
    void setTitle(String title) {

    }

    @Override
    void setAddedOn() {

    }

    @Override
    int getPlayingTime() {
        return playingTime;
    }

    @Override
    String getAuthor() {
        return null;
    }

    @Override
    void setPlayingTime(int playingTime) {

    }
}
