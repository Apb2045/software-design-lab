import java.util.Date;

/**
 * Created by Alex on 10/14/15.
 */
public class video extends multiMediaItem {

    private String director;

    public video(String my_id, String my_title, Date my_addedOn, int my_playingTime, String my_director){
        super(my_id, my_title, my_addedOn, my_playingTime);
        id = my_id;
        title = my_title;
        addedOn = my_addedOn;
        playingTime = my_playingTime;
        director = my_director;
    }

    @Override
    public String getDirector(){
        return director;
    }

    @Override
    String getArtist() {
        return null;
    }

    @Override
    public int compareTo(item o) {
        return 0;
    }

    @Override
    String getId() {
        return id;
    }

    @Override
    String getTitle() {
        return title;
    }

    @Override
    Date getAddedOn() {
        return addedOn;
    }

    @Override
    void setId(String id) {

    }

    @Override
    void setTitle(String title) {

    }

    @Override
    void setAddedOn() {

    }

    @Override
    int getPlayingTime() {
        return 0;
    }

    @Override
    String getAuthor() {
        return null;
    }

    @Override
    void setPlayingTime(int playingTime) {

    }
}
