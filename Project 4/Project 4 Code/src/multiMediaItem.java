import java.util.Date;

/**
 * Created by Alex on 10/14/15.
 */
abstract class multiMediaItem extends item {

    protected int playingTime;

    public multiMediaItem(String my_id, String my_title, Date my_addedOn, int my_playingTime){
        super(my_id, my_title, my_addedOn);
        id = my_id;
        title = my_title;
        addedOn = my_addedOn;
        playingTime = my_playingTime;
    }

    //declare the functions for multimedia items.
    abstract int getPlayingTime();
    abstract void setPlayingTime(int playingTime);

}
