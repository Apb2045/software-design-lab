import java.util.Random;

/**
 * Created by Alex on 9/16/15.
 */
public class Hare extends Player {

    private Random rn = new Random();


    Hare(){
        ;}

    @Override
    public void setPosition() {
        super.setPosition();

        int answer = rn.nextInt(10) + 1;

        if( answer == 1 || answer == 2){
            step = 0;
        }
        else if( answer == 3 || answer == 4){
            step = 9;
        }
        else if(answer == 5){
            step = -12;
        }
        else if( answer == 6 || answer == 7 || answer == 8 )
        {
            step = 1;
        }
        else{
            step = -2;
        }

        position = position + step;

        if(position < 1){
            position = 1;
        }
        if(position > 70){
            position = 70;
        }
    }
}
