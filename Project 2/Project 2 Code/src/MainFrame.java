/**
 * Created by Alex on 9/16/15.
 */

import java.awt.Panel;
import javax.swing.*;

public class MainFrame extends JFrame{
    //Simple Object class used to add our MainPanel to a JFrame so that it can
    //be displayed without the use of an applet.

    MainPanel mPanel = new MainPanel();

    MainFrame(){

        setTitle("Tortoise and Hare");
        mPanel.setLayout(null);
        setSize(700,625);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); /*exits when we close application*/
        add(mPanel); //adds our main panel object
        //setVisible(true); //Shows mainframe
    }

    public static void main(String[] args){
        new MainFrame();
    }
}
