import java.util.Random;

/**
 * Created by Alex on 9/16/15.
 */
public class Tortoise extends Player{

    private Random rn = new Random();

    Tortoise(){
            ;}

    @Override
    public void setPosition() {
        super.setPosition();

        int answer = rn.nextInt(10) + 1;

        if( answer >= 1 && answer <= 5){
            step = 3;
        }
        else if( answer == 6 || answer == 7){
            step = -6;
        }
        else{
            step = 1;
        }

        position = position + step;

        if(position < 1){
            position = 1;
        }
        if(position > 70){
            position = 70;
        }

    }
}
