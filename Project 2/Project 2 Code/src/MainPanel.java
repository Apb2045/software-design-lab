/**
 * Created by Alex on 9/16/15.
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class MainPanel extends JPanel{

    Image hareImage;
    Image tortoiseImage;
    Image backgroundImage;

    Tortoise currentTortoise = new Tortoise();
    Hare currentHare = new Hare();

    boolean declared = false;


    public MainPanel(){

        ImageIcon Tortoise_IMG_ICON = new javax.swing.ImageIcon(getClass().getResource("resources/tortoise.png"));
        tortoiseImage = Tortoise_IMG_ICON.getImage();

        ImageIcon Hare_IMG_ICON = new javax.swing.ImageIcon(getClass().getResource("resources/hare.png"));
        hareImage = Hare_IMG_ICON.getImage();

        ImageIcon Background_IMG_ICON = new javax.swing.ImageIcon(getClass().getResource("resources/background.png"));
        backgroundImage = Background_IMG_ICON.getImage();

        prepareImage(tortoiseImage, this);

        currentTortoise.setX(10);
        currentTortoise.setY(325);

        prepareImage(hareImage, this);
        currentHare.setX(10);
        currentHare.setY(325);

        JButton start = new JButton("Re/Start");
        start.addActionListener(new Start());
        add(start);
        start.setBounds(15, 550, 100, 30);

        JButton quit = new JButton("Quit");
        quit.addActionListener(new Quit());
        add(quit);
        quit.setBounds(125, 550, 100, 30);

        setDoubleBuffered(true);
    }

    class Quit implements ActionListener{
        public void actionPerformed(ActionEvent e){
            System.exit(0);
        }
    }

    class Start implements  ActionListener{
        public void actionPerformed(ActionEvent e){

            JLabel begin = new JLabel("Boom There Off!");
            begin.setBounds(250, 550, 250, 30);

            //if(currentHare.getPosition() == 1 && currentTortoise.getPosition() == 1)
                add(begin);

            Timer timer = new Timer(1400, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    remove(begin);
                }
            }); timer.start();

            if(declared == false) {
                new Timer(100, paintTimer).start();
                declared = true;
            }

            if(currentHare.getWinner() == true || currentTortoise.getWinner() == true){
                currentTortoise.resetPosition();
                currentHare.resetPosition();
                currentTortoise.setWinner();
                currentHare.setWinner();
            }
        }
    }

    public void paint(Graphics g){

        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;

        g2d.drawImage(backgroundImage,0,0,this);

        g2d.drawImage(tortoiseImage, currentTortoise.getX(), currentTortoise.getY(), this);

        g2d.drawImage(hareImage, currentHare.getX(), currentHare.getY(), this);

        Toolkit.getDefaultToolkit().sync();

        g.dispose();

    }

    Action paintTimer = new AbstractAction(){
        public void actionPerformed(ActionEvent e) {

            //label
            JLabel ouch = new JLabel("The Tortoise bit the Hare...Ouch!");
            JLabel hareWins = new JLabel("Hare wins. Yuch.");
            JLabel tortoiseWins = new JLabel("TORTOISE WINS!!! YAY!!!");
            JLabel itsATie = new JLabel("Meh! its a Tie...");
            JLabel numHareWins = new JLabel("Hare: " + currentHare.getNumWins());
            JLabel numTortoiseWins = new JLabel("Tortoise: " + currentTortoise.getNumWins());


            ouch.setBounds(250, 550, 250, 30);
            hareWins.setBounds(250, 550, 250, 30);
            tortoiseWins.setBounds(250, 550, 250, 30);

            numHareWins.setBounds(550, 525, 250, 30);
            numTortoiseWins.setBounds(550, 575, 250, 30);

            {
                add(numHareWins);

                add(numTortoiseWins);

                Timer timer = new Timer(40, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        remove(numHareWins);
                        remove(numTortoiseWins);
                    }
                });
                timer.start();
            }

            if((currentTortoise.getPosition() == currentHare.getPosition()) && (currentTortoise.getPosition() != 1 && currentHare.getPosition() != 1) && currentTortoise.getWinner() == false) {
                add(ouch);
                Timer timer = new Timer(400, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        remove(ouch);
                    }
                }); timer.start();
            }

            if(currentHare.getWinner() == true && currentTortoise.getWinner() == true)
            {
                add(itsATie);

                Timer timer = new Timer(50, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        remove(itsATie);
                    }
                }); timer.start();

            }

            else if(currentHare.getWinner() == true){


                add(hareWins);

                Timer timer = new Timer(50, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        remove(hareWins);
                    }
                }); timer.start();
            }

            else if(currentTortoise.getWinner() == true){

                add(tortoiseWins);

                Timer timer = new Timer(50, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        remove(tortoiseWins);
                    }
                }); timer.start();
            }

            if(currentHare.getWinner() == false && currentTortoise.getWinner() == false) {
                currentTortoise.setPosition();
                currentHare.setPosition();

                currentTortoise.setX((int) (10 + currentTortoise.getPosition() * 7.8));
                currentTortoise.setY((int) (325 + currentTortoise.getPosition() * (-4.7)));

                currentHare.setX((int) (10 + currentHare.getPosition() * 7.8));
                currentHare.setY((int) (325 + currentHare.getPosition() * (-4.7)));

                currentTortoise.setWinner();
                currentHare.setWinner();
            }
            repaint();

        }
    };
}