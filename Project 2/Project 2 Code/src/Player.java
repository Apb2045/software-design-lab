import java.util.Random;

/**
 * Created by Alex on 9/16/15.
 */
public class Player {

    private int x;
    private int y;
    protected int position;
    protected int step;
    private boolean winner;
    protected int numWins;

    Player(){
        position = 1;
        winner = false;
        step = 0;
        numWins = 0;
    }

    public void setPosition(){

    }

    public void resetPosition(){
        position = 1;
    }

    public int getPosition(){
        return position;
    }

    public int getStep(){
        return step;
    }

    public void setWinner(){
        if(this.getPosition() == 70)
        {
            winner = true;
            numWins++;
        }
        else{winner = false;}

    }

    public int getNumWins(){ return numWins;}

    public boolean getWinner(){
        return winner;
    }

    public int getX(){
        return x;
    }

    public void setX(int x2){
        x = x2;
    }

    public int getY(){
        return y;
    }

    public void setY(int y2){
        y = y2;
    }
}
