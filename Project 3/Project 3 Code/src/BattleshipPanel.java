import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Represents a BattleshipGrid graphically.
 */
public class BattleshipPanel extends JPanel
{
    // the width and height of this panel
    private final int PANEL_WIDTH;
    private final int PANEL_HEIGHT;

    private BattleshipGrid grid;
    private HitMissPanel hitMissPanel;

    private int numHits;
    private int numMisses;

    private boolean hintRequested = false;

    public BattleshipPanel(int width, int height, BattleshipGrid battleshipGrid, HitMissPanel hmp)
    {
        PANEL_WIDTH = width;
        PANEL_HEIGHT = height;
        grid = battleshipGrid;
        hitMissPanel = hmp;

        numHits = 0;
        numMisses = 0;

        setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));

        JButton start = new JButton("Reset");
        start.addActionListener(new Reset());
        add(start);
        start.setBounds(15, 550, 100, 30);

        JButton quit = new JButton("Quit");
        quit.addActionListener(new Quit());
        add(quit);
        quit.setBounds(125, 550, 100, 30);

        JButton hint = new JButton("Hint");
        hint.addActionListener(new Hint());
        add(hint);
        hint.setBounds(125, 550, 100, 30);

        addMouseListener(
                new MouseAdapter() {
                    public void mouseClicked(MouseEvent e) {
                        // attack the grid based on where the user clicked
                        attackGrid(e.getX(), e.getY());

                        // update the graphics
                        repaint();

                        // have all of the ships been destroyed?
                        if (grid.allDestroyed()) {
                            JOptionPane.showMessageDialog(null, "You win!");
                            //System.exit(0);
                            grid.initializeGrid();
                            grid.randomShipLengths();
                            grid.calculateTotalHitsRequired();
                            grid.placeAllShips();
                            grid.resetHitCount();
                            System.out.print("Reset\n\n");
                            numMisses = 0;
                            numHits = 0;
                            hitMissPanel.setStats(numHits,numMisses);
                            repaint();
                        }
                    }
                }
        );
    }

    class Quit implements ActionListener{
        public void actionPerformed(ActionEvent e)
        {
            System.exit(0);
        }
    }

    class Reset implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            grid.initializeGrid();
            grid.randomShipLengths();
            grid.calculateTotalHitsRequired();
            grid.placeAllShips();
            grid.resetHitCount();
            System.out.print("Reset\n\n");
            numMisses = 0;
            numHits = 0;
            hitMissPanel.setStats(numHits,numMisses);
            repaint();

        }
    }
    class Hint implements ActionListener{
            public void actionPerformed(ActionEvent e){
                System.out.print("Hint\n\n");
                hintRequested = true;
                repaint();
            }
    }

    public void paintComponent(Graphics g)
    {
        setBackground(Color.BLACK);
        super.paintComponent(g);

        drawGridLines(g);
        drawGridCells(g);

        if(hintRequested)
        {
            drawGridHint(g);
            hintRequested = false;
        }
    }

    /**
     * Draw grid lines.
     *
     * @param    g    the graphics context on which to draw the grid lines
     */
    private void drawGridLines(Graphics g)
    {
        g.setColor(Color.GRAY);

        // draw vertical grid lines
        for (int x = 0; x < PANEL_WIDTH; x += PANEL_WIDTH / grid.NUM_COLS)
        {
            g.drawLine(x, 0, x, PANEL_HEIGHT - 1);
        }

        // draw horizontal grid lines
        for (int y = 0; y < PANEL_HEIGHT; y += PANEL_HEIGHT / grid.NUM_ROWS)
        {
            g.drawLine(0, y, PANEL_WIDTH - 1, y);
        }
    }

    private void drawGridHint(Graphics g)
    {
        for (int row = 0; row < grid.NUM_ROWS; row++)
        {
            for (int col = 0; col < grid.NUM_COLS; col++)
            {
                if(grid.getCell(row,col) == 1)
                {
                    g.setColor(Color.WHITE);
                    g.fillOval(col * 60 + 2, row * 60 + 2, 56, 56);
                }
            }
        }
    }



    /**
     * Draws the grid cells such that empty cells are represented by
     * black squares, misses by blue squares, and hits by red squares.
     *
     * @param    g    the graphics context on which to draw the grid cells
     */
    private void drawGridCells(Graphics g)
    {
        // iterate over the entire grid
        for (int row = 0; row < grid.NUM_ROWS; row++)
        {
            for (int col = 0; col < grid.NUM_COLS; col++)
            {
                if(grid.getCell(row,col) == 2)
                {
                    g.setColor(new Color(180,0,0));
                    g.fillOval(col * 60 + 2, row * 60 + 2, 56, 56);
                }
                if(grid.getCell(row,col) == 3)
                {
                    g.setColor(new Color(0,180,0));
                    g.fillOval(col * 60 + 2, row * 60 + 2, 56, 56);
                }
                if(hintRequested)
                {
                    if(grid.getCell(row,col) == 1)
                    {
                        g.setColor(Color.lightGray);
                        g.fillOval(col * 60 + 2, row * 60 + 2, 56, 56);
                    }

                }
            }
        }
        hintRequested = false;
    }



    /**
     * Determines which grid cell the user clicked on and then attacks
     * that grid cell. If the user has already attacked the cell that
     * was clicked on, this method does not attack the cell. This
     * method also updates the total number of hits and misses and
     * updates the hit/miss panel accordingly.
     *
     * @param    mouseX    the x-coordinate of the user's mouse click
     * @param    mouseY    the y-coordinate of the user's mouse click
     */
    private void attackGrid(int mouseX, int mouseY)
    {
        int row = mouseY/60;
        int column = mouseX/60;

        if(grid.getCell(row, column) == 1) {
            numHits++;}
        else if(grid.getCell(row, column) == 0){ numMisses++;}

        hitMissPanel.setStats(numHits, numMisses);
        grid.attack(row, column);
        grid.printGrid();
        System.out.print("grid (" + (int) (mouseX/60) + ", " + (int)(mouseY/60) +  ")\n\n");
    }
}