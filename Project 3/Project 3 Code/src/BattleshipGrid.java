import java.util.Random;

/**
 * This class represents a Battleship game grid.
 */
public class BattleshipGrid
{
    public static final int OUT_OF_BOUNDS = -1;

    private int HITS = 0;

    // values to be used in the 2D grid
    public static final int EMPTY = 0;
    public static final int SHIP = 1;
    public static final int HIT = 2;
    public static final int MISS = 3;

    public static final int NUMBER_OF_SHIPS = 6;

    // directions used for shape placement
    private static final int DIRECTION_RIGHT = 0;
    private static final int DIRECTION_DOWN = 1;

    // lengths of the various ships in the game
    private int SHIP_1_LENGTH = 99;
    private int SHIP_2_LENGTH = 99;
    private int SHIP_3_LENGTH = 99;
    private int SHIP_4_LENGTH = 99;
    private int SHIP_5_LENGTH = 99;
    private int SHIP_6_LENGTH = 99;

    // array of all ship lengths
    private int [] SHIP_LENGTHS =
    {
        SHIP_1_LENGTH,
        SHIP_2_LENGTH,
        SHIP_3_LENGTH,
        SHIP_4_LENGTH,
        SHIP_5_LENGTH,
        SHIP_6_LENGTH
    };

    // the total number of hits required to destroy all ships    
    private int totalHitsRequired;

    // the number of rows and columns in this grid
    public static final int NUM_ROWS = 10;
    public static final int NUM_COLS = 10;

    // ::: DECLARE YOUR 2D ARRAY NAMED grid HERE
    int[ ][ ] grid = new int[NUM_ROWS][NUM_COLS];


    /**
     * Creates a new BattleshipGrid instance and randomly places
     * ships on the grid.
     *
     * @param    numRows    the number of rows in this grid
     * @param    numCols    the number of columns in this grid
     */
    public BattleshipGrid()
    {
        // initialize the grid so all cells are EMPTY
        initializeGrid();

        randomShipLengths();

        // determine the total number of hits required to win
        calculateTotalHitsRequired();

        // randomly place all of the ships on the grid
        placeAllShips();
    }

    public void randomShipLengths(){

        Random random = new Random();
        int rand;
        for(int i = 0; i < NUMBER_OF_SHIPS; i++){
            rand = random.nextInt(3) + 2;
            SHIP_LENGTHS[i] = rand;
        }
    }

    /**
     * Initializes the game grid to size [NUM_ROWS][NUM_COLS]
     * and sets each element of the grid to the constant EMPTY.
     */

    public void resetHitCount()
    {
        HITS = 0;
    }
    public void initializeGrid()
    {
        // ::: MAKE grid A 2D ARRAY OF THE APPROPRIATE DIMENSIONS AS
        //     SPECIFIED IN THE METHOD DESCRIPTION. REMEMBER THAT YOU
        //     HAVE ALREADY DECLARED grid AS AN INSTANCE VARIABLE AND
        //     THAT YOU SHOULD NOT REDECLARE IT HERE.



        // ::: INITIALIZE ALL ELEMENTS OF grid TO THE VALUE EMPTY
        for(int i = 0; i < grid.length; i++)
        {
            for(int j = 0; j < grid[i].length; j++)
            {
                grid[i][j] = 0;
            }
        }



    }
    /**
     * Calculates the total number of hits required to
     * destroy all ships and win the game.
     */
    public  void calculateTotalHitsRequired()
    {
        totalHitsRequired = 0;

        // add all the ship lengths together to get
        // the total number of hits required
        for (int length : SHIP_LENGTHS)
        {
            totalHitsRequired += length;
        }
        System.out.print(totalHitsRequired);
    }

    /**
     * Places all of the game's ships onto the grid.
     */
    public void placeAllShips()
    {
        // ::: FILL IN THIS CODE
        Random random = new Random();
        boolean placed = false;
        for(int j =0; j < NUMBER_OF_SHIPS; j++) {
            while (!placed) {
                int rr = random.nextInt(10);
                int rc = random.nextInt(10);
                int rd = random.nextInt(2);
                boolean isClear = true;

                if (rd == DIRECTION_RIGHT) {
                    if (inBounds(rr, rc + SHIP_LENGTHS[j])) {
                        for (int i = 0; i < SHIP_LENGTHS[j]; i++) {
                            if (grid[rr][rc + i] != 0) {
                                isClear = false;
                            }
                        }
                        if (isClear) {
                            for (int i = 0; i < SHIP_LENGTHS[j]; i++) {
                                grid[rr][rc + i] = 1;
                            }
                            placed = true;
                        }
                    }
                }
                else
                {
                    if (inBounds(rr + SHIP_LENGTHS[j], rc)) {
                        for (int i = 0; i < SHIP_LENGTHS[j]; i++) {
                            if (grid[rr + i][rc] != 0) {
                                isClear = false;
                            }
                        }
                        if (isClear) {
                            for (int i = 0; i < SHIP_LENGTHS[j]; i++) {
                                grid[rr + i][rc] = 1;
                            }
                            placed = true;
                        }
                    }
                }
            }
            placed = false;
        }
    }

    /**
     * Attacks the grid cell at the specified row and column.
     * If the grid cell contains:
     *   - SHIP: the value of the cell is set to HIT
     *   - HIT: the value of the cell does not change
     * Otherwise, the value of the cell is set to MISS.
     *
     * This method returns true if the attack resulted in a ship being hit,
     * and false otherwise.
     *
     * Note: this method also returns true if a cell that has already
     * been hit is attacked.
     *
     * @param    row    the row of the cell to attack
     * @param    col    the column of the cell to attack
     *
     * @return   true if the attack results in a ship being hit (even
     *           if the ship at that cell has already been hit),
     *           false otherwise
     */
    public boolean attack(int row, int col)
    {
        // ::: FILL IN THIS CODE
        int r = row;
        int c = col;
        if(grid[r][c] == 0)
        {
            grid[r][c] = 3;
            return false;
        }
        else if(grid[r][c] == 1)
        {
            grid[r][c] = 2;
            HITS++;
            System.out.print("HITS " + HITS + "\n\n");
            return true;
        }
        else if(grid[r][c] == 2)
        {
            return true;
        }
        else
        {
            return false;
        }

    }



    /**
     * Returns true if all of the ships have been destroyed, and
     * false otherwise.
     *
     * @return   true if all ships have been destroyed, false otherwise
     */
    public boolean allDestroyed()
    {
        if(HITS == totalHitsRequired)
            return true;

        return false;
    }



    /**
     * Determine if the cell at the specified row and column is in
     * the grid's bounds.
     *
     * @param    row    the cell's row
     * @param    col    the cell's column
     *
     * @return   true if the cell is in bounds, false otherwise
     */
    private boolean inBounds(int row, int col)
    {
        return ((row >= 0) && (row < NUM_ROWS) &&
                (col >= 0) && (col < NUM_COLS));
    }

    /**
     * Returns the value of the cell at [row][col] if the cell is in
     * bounds. Otherwise, returns OUT_OF_BOUNDS.
     *
     * @param    row    the cell's row
     * @param    col    the cell's column
     *
     * @return   if in bounds, the value of the cell at [row][col],
     *           otherwise OUT_OF_BOUNDS
     */
    public int getCell(int row, int col)
    {
        if (inBounds(row, col))
            return grid[row][col];
        else
            return OUT_OF_BOUNDS;
    }

    /**
     * Prints the grid.
     */
    public void printGrid()
    {
        // ::: FILL IN THIS CODE
        for(int i = 0; i < grid.length; i++)
        {
            for(int j = 0; j < grid[i].length; j++)
            {
                System.out.print(grid[i][j]);
                if(j < grid[i].length - 1) System.out.print(" ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println();
    }
}